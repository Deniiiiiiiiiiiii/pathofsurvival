import java.time.LocalDate;

/**
 * Created: 06.03.2023
 *
 * @author Barbos Ioana-Denisa (barbo)
 */
public record Player(String alias, int scorePerMinute, int wins, int looses, LocalDate lastMatchDate) {
    @Override
    public String toString() {
        return "-------------------------" + '\n' +
                "alias: " + alias + '\n' +
                "scorePerMinute: " + scorePerMinute + '\n' +
                "wins: " + wins + '\n' +
                "loses: " + looses + '\n' +
                "lastMatchDate: " + lastMatchDate  + '\n' + "-------------------------";
    }

    public Player changePlayer(int newWins, int newLooses, int newScorePerMinute){
        return new Player(alias, newScorePerMinute, newWins, newLooses, lastMatchDate);
    }
}
