import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.time.LocalDate;
import java.util.*;

/**
 * Created: 06.03.2023
 *
 * @author Barbos Ioana-Denisa (barbo)
 */
public class PathOfSurvival {
    private static List<Player> players = new ArrayList<>();

    public static void main(String[] args) {
        readPlayers("src/main/resources/playerdata.bin");



        findPlayer("HunterKiller11111elf", 1337, 0, 15000);
        findPlayer("CyberBob", 1, 354, 2);
        findPlayer("ShadowDeath42", 0, 400, 3);

        Comparator<Player> byScore = Comparator.comparingInt(Player::scorePerMinute).reversed();
        players.sort(byScore);

        for (Player player : players) {
            System.out.println(player);
            write("src/main/resources/playerdata.bin", player);
        }


        /**
        System.out.println(" ");
        System.out.println("veränderte Players: ");
        findPlayer(players, "HunterKiller11111elf", 1337, 0, 15000);
        System.out.println(getPlayer(players,"HunterKiller11111elf"));
        findPlayer(players, "CyberBob", 1, 354, 2);
        System.out.println(getPlayer(players,"CyberBob"));
        findPlayer(players, "ShadowDeath42", 0, 400, 3);
        System.out.println(getPlayer(players,"ShadowDeath42"));
         **/

    }

    public static void readPlayers(String filename){

        try(RandomAccessFile randomAccessFile = new RandomAccessFile(filename, "r")) {

            randomAccessFile.skipBytes(1345);
            short playerCount = randomAccessFile.readShort();

            for (int i = 0; i < playerCount; i++) {

                short playerDataLength = randomAccessFile.readShort();

                String alias = randomAccessFile.readUTF();
                int year = randomAccessFile.readInt();
                int month = randomAccessFile.readInt();
                int day = randomAccessFile.readInt();
                int scorePerMinute = randomAccessFile.readInt();
                int wins = randomAccessFile.readInt();
                int looses = randomAccessFile.readInt();
                players.add(new Player(alias, scorePerMinute, wins, looses, LocalDate.of(year, month, day)));
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void write(String filename, Player player){
        try(RandomAccessFile randomAccessFile = new RandomAccessFile(filename, "rw")){

            randomAccessFile.skipBytes(1345);
            short playerCount = randomAccessFile.readShort();

            for (int i = 0; i < playerCount; i++) {

                randomAccessFile.writeUTF(player.alias());
                randomAccessFile.writeInt(player.lastMatchDate().getYear());
                randomAccessFile.writeInt(player.lastMatchDate().getMonthValue());
                randomAccessFile.writeInt(player.lastMatchDate().getDayOfMonth());
                randomAccessFile.writeInt(player.scorePerMinute());
                randomAccessFile.writeInt(player.wins());
                randomAccessFile.writeInt(player.looses());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void findPlayer(String alias, int newWins, int newLooses, int newScorePerMinute){
        for (int i = 0; i < players.size(); i++) {
            Player player = players.get(i);
            if (player.alias().equals(alias)){
                Player newPlayer = player.changePlayer(newWins, newLooses, newScorePerMinute);
                players.set(i, newPlayer);
                break;
            }
        }
    }

    public static Player getPlayer(String alias){
        for (Player player: players){
            if (player.alias().equals(alias)){
                return player;
            }
        }
        return null;
    }
}
